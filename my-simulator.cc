#include "ns3/core-module.h"
#include <iostream>
#include "ns3/internet-module.h"
#include "ns3/network-module.h"
#include "ns3/applications-module.h"
#include "ns3/point-to-point-module.h"
#include "ns3/csma-module.h"
#include "ns3/ipv4-address.h"
#include "ns3/flow-monitor-helper.h"
#include "ns3/flow-monitor-module.h"

using namespace ns3;

NS_LOG_COMPONENT_DEFINE("MySimulator");

int main(int argc, char* argv[]) {
  LogComponentEnable ("MySimulator", LOG_LEVEL_INFO);
  uint32_t SentPackets = 0;
  uint32_t ReceivedPackets = 0;
  uint32_t LostPackets = 0;

  // Inizializzazione del sistema di temporizzazione
  Time::SetResolution(Time::NS);
  // Abilitazione del log level dell'UdpEchoClientApplication e dell'UdpEchoServerApplication
  LogComponentEnable("UdpEchoClientApplication", LOG_LEVEL_INFO);
  LogComponentEnable("UdpEchoServerApplication", LOG_LEVEL_INFO);  
  
  // Creazione 4 nodi
  NodeContainer nodes;
  nodes.Create(4);

  // Configurazione della connessione punto-punto tra i nodi 0 e 1
  PointToPointHelper p2pHelper;
  p2pHelper.SetDeviceAttribute("DataRate", StringValue("1000Mbps"));
  p2pHelper.SetChannelAttribute("Delay", StringValue("2ms"));
  NetDeviceContainer p2pDevices01;
  p2pDevices01 = p2pHelper.Install(nodes.Get(0), nodes.Get(1));

  // Configurazione della connessione punto-punto tra i nodi 2 e 3
  // Stesse caratteristiche di velocità e ritardo dei nodi 0 e 1
  NetDeviceContainer p2pDevices23;
  p2pDevices23 = p2pHelper.Install(nodes.Get(2), nodes.Get(3));

  // Connessione CSMA nodo 0 e 3, 
  CsmaHelper csmaHelper;
  csmaHelper.SetChannelAttribute("DataRate", StringValue("1000Mbps"));
  csmaHelper.SetChannelAttribute("Delay", TimeValue(NanoSeconds(6560)));
  
  NetDeviceContainer csmaDevices;
  NodeContainer csmaNodes;
  csmaNodes.Add(nodes.Get(0));
  csmaNodes.Add(nodes.Get(3));
  csmaDevices = csmaHelper.Install(csmaNodes);

  // Installa protocolli di rete
  InternetStackHelper stack;
  stack.Install(nodes);

 // Creazione degli oggetti di indirizzi IPv4
  Ipv4AddressHelper ipv4;

  // Assegnazione dell'indirizzo IPv4 ai nodi 0 e 1
  ipv4.SetBase("10.1.1.0", "255.255.255.0");
  Ipv4InterfaceContainer p2pInterfaces01 = ipv4.Assign(p2pDevices01);

  // Assegnazione dell'indirizzo IPv4 ai nodi 2 e 3
  ipv4.SetBase("10.1.2.0", "255.255.255.0");
  Ipv4InterfaceContainer p2pInterfaces23 = ipv4.Assign(p2pDevices23);

  // Assegnazione dell'indirizzo IPv4 ai nodi 0 e 3 della rete CSMA
  ipv4.SetBase("10.1.3.0", "255.255.255.0");
  Ipv4InterfaceContainer csmaInterfaces = ipv4.Assign(csmaDevices);

  // Creazione del server 1 e installazione sul nodo 1
  UdpEchoServerHelper echoServer1(9); // il server ascolta sulla porta 9
  ApplicationContainer serverApps1 = echoServer1.Install(nodes.Get(1));
  serverApps1.Start(Seconds(1.0));
  serverApps1.Stop(Seconds(250.0)); // il server rimane attivo 

  // Creazione del server 2 e installazione sul nodo 3
  UdpEchoServerHelper echoServer2(10); // il server ascolta sulla porta 10
  ApplicationContainer serverApps2 = echoServer2.Install(nodes.Get(3));
  serverApps2.Start(Seconds(1.0));
  serverApps2.Stop(Seconds(200.0)); // il server rimane attivo 

  // Creazione del client 1 e installazione sul nodo 0
  UdpEchoClientHelper echoClient1(p2pInterfaces01.GetAddress(1), 9); // il client si connette al server 1 sulla porta 9
  echoClient1.SetAttribute("MaxPackets", UintegerValue(55)); // il client invia pacchetti
  echoClient1.SetAttribute("Interval", TimeValue(Seconds(0.5))); // intervallo tra i pacchetti
  echoClient1.SetAttribute("PacketSize", UintegerValue(1024)); // dimensione del pacchetto
  ApplicationContainer clientApps1 = echoClient1.Install(nodes.Get(0));
  clientApps1.Start(Seconds(1.0));
  clientApps1.Stop(Seconds(150.0)); // il client invia pacchetti 

  // Creazione del client 2 e installazione sul nodo 2
  UdpEchoClientHelper echoClient2(p2pInterfaces23.GetAddress(1), 10); // il client si connette al server 2 sulla porta 10
  echoClient2.SetAttribute("MaxPackets", UintegerValue(80)); // il client invia pacchetti
  echoClient2.SetAttribute("Interval", TimeValue(Seconds(0.3))); // intervallo tra i pacchetti
  echoClient2.SetAttribute("PacketSize", UintegerValue(1024)); // dimensione del pacchetto
  ApplicationContainer clientApps2 = echoClient2.Install(nodes.Get(2));
  clientApps2.Start(Seconds(2.0));
  clientApps2.Stop(Seconds(150.0)); // il client invia pacchetti 

  // Creazione del client 3 e installazione sul nodo 0 (limite di risorse)
  UdpEchoClientHelper echoClient3(csmaInterfaces.GetAddress(1), 10); // il client si connette al server 2 sulla porta 10
  echoClient3.SetAttribute("MaxPackets", UintegerValue(150)); // il client invia pacchetti
  echoClient3.SetAttribute("Interval", TimeValue(Seconds(0.1))); // intervallo tra i pacchetti
  echoClient3.SetAttribute("PacketSize", UintegerValue(1024)); // dimensione del pacchetto
  ApplicationContainer clientApps3 = echoClient3.Install(nodes.Get(0));
  clientApps3.Start(Seconds(1.0));
  clientApps3.Stop(Seconds(150.0)); // il client invia pacchetti 

    FlowMonitorHelper flowmon;
  Ptr<FlowMonitor> monitor = flowmon.InstallAll();

  Simulator::Stop(Seconds(30.0));
  Simulator::Run();

  int j = 0;
  float AvgThroughput = 0;
  Time Jitter;
  Time Delay;



  Ptr<Ipv4FlowClassifier> classifier = DynamicCast<Ipv4FlowClassifier> (flowmon.GetClassifier ());
  std::map<FlowId, FlowMonitor::FlowStats> stats = monitor->GetFlowStats ();

  for (std::map<FlowId, FlowMonitor::FlowStats>::const_iterator iter = stats.begin (); iter != stats.end (); ++iter)
    {
	  Ipv4FlowClassifier::FiveTuple t = classifier->FindFlow (iter->first);

  NS_LOG_UNCOND("----Flow ID:" <<iter->first);
  NS_LOG_UNCOND("Src Addr" <<t.sourceAddress << "Dst Addr "<< t.destinationAddress);
  NS_LOG_UNCOND("Sent Packets = " <<iter->second.txPackets);
  NS_LOG_UNCOND("Received Packets = " <<iter->second.rxPackets);
  NS_LOG_UNCOND("Lost Packets = " <<iter->second.txPackets-iter->second.rxPackets);
  NS_LOG_UNCOND("Packet delivery ratio = " <<iter->second.rxPackets*100/iter->second.txPackets << "%");
  NS_LOG_UNCOND("Packet loss ratio = " << (iter->second.txPackets-iter->second.rxPackets)*100/iter->second.txPackets << "%");
  NS_LOG_UNCOND("Delay = " <<iter->second.delaySum);
  NS_LOG_UNCOND("Jitter = " <<iter->second.jitterSum);
  NS_LOG_UNCOND("Throughput = " <<iter->second.rxBytes * 8.0/(iter->second.timeLastRxPacket.GetSeconds()-iter->second.timeFirstTxPacket.GetSeconds())/1024<<"Kbps");

  SentPackets = SentPackets +(iter->second.txPackets);
  ReceivedPackets = ReceivedPackets + (iter->second.rxPackets);
  LostPackets = LostPackets + (iter->second.txPackets-iter->second.rxPackets);
  AvgThroughput = AvgThroughput + (iter->second.rxBytes * 8.0/(iter->second.timeLastRxPacket.GetSeconds()-iter->second.timeFirstTxPacket.GetSeconds())/1024);
  Delay = Delay + (iter->second.delaySum);
  Jitter = Jitter + (iter->second.jitterSum);



j = j + 1;

}

AvgThroughput = AvgThroughput/j;
NS_LOG_UNCOND("--------Total Results of the simulation----------"<<std::endl);
NS_LOG_UNCOND("Total sent packets  = " << SentPackets);
NS_LOG_UNCOND("Total Received Packets = " << ReceivedPackets);
NS_LOG_UNCOND("Total Lost Packets = " << LostPackets);
NS_LOG_UNCOND("Packet Loss ratio = " << ((LostPackets*100)/SentPackets)<< "%");
NS_LOG_UNCOND("Packet delivery ratio = " << ((ReceivedPackets*100)/SentPackets)<< "%");
NS_LOG_UNCOND("Average Throughput = " << AvgThroughput<< "Kbps");
NS_LOG_UNCOND("End to End Delay = " << Delay);
NS_LOG_UNCOND("End to End Jitter delay = " << Jitter);
NS_LOG_UNCOND("Total Flod id " << j);

monitor->SerializeToXmlFile ("results.txt",false,true);

  }